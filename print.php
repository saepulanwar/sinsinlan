<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>
	<style>
		table {
			border-collapse:collapse;
			table-layout:fixed;width: 630px;
		}
		table td {
			word-wrap:break-word;
			width: 20%;
		}
	</style>
</head>
<body>
	<?php
	// Load file koneksi.php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

	if(isset($_POST['filter']) && ! empty($_POST['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
        $filter = $_POST['filter']; // Ambil data filder yang dipilih user

        if($filter == '1'){ // Jika filter nya 1 (per tanggal)
            $tgl = date('d-m-y', strtotime($_POST['tanggal']));

            echo '<b>Data Transaksi Tanggal '.$tgl.'</b><br /><br />';
            // echo '<a href="print.php?filter=1&tanggal='.$_POST['tanggal'].'">Cetak PDF</a><br /><br />';
            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
            // $query = "SELECT * FROM transaksi WHERE DATE(tgl)='".$_GET['tanggal']."'"; // Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
        }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
            $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

            echo '<b>Data Transaksi Bulan '.$nama_bulan[$_POST['bulan']].' '.$_POST['tahun'].'</b><br /><br />';
            echo '<a href="print.php?filter=2&bulan='.$_POST['bulan'].'&tahun='.$_POST['tahun'].'">Cetak PDF</a><br /><br />';

            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";

            $query = "SELECT * FROM barang_keluar WHERE MONTH(tgl)='".$_POST['bulan']."' AND YEAR(tgl)='".$_POST['tahun']."'"; // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
        }else{ // Jika filter nya 3 (per tahun)
            echo '<b>Data Transaksi Tahun '.$_POST['tahun'].'</b><br /><br />';
            // echo '<a href="print.php?filter=3&tahun='.$_POST['tahun'].'">Cetak PDF</a><br /><br />';
            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";

            // $query = "SELECT * FROM transaksi WHERE YEAR(tgl)='".$_GET['tahun']."'"; // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
        }
    }else{ // Jika user tidak mengklik tombol tampilkan
        echo '<b>Semua Data Transaksi</b><br /><br />';
        // echo '<a href="print.php">Cetak PDF</a><br /><br />';
        $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar`";
        $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar`";
        $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar`";
        $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar`";
        $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar`";
        $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar`";

        $query = "SELECT * FROM barang_keluar ORDER BY tgl"; // Tampilkan semua data transaksi diurutkan berdasarkan tanggal
    }
	?>
	<table>
        <tr>
            <td class="left">Nama Barang Terjual:</td>
        </tr>
        <tr>
            <td class="left">Hoodie Grade A :</td>
            <td id="hoodie_a">
                <?php 
                        $sql = mysqli_query($conn, $query_ha); // Eksekusi/Jalankan query dari variabel $query
                        $row = mysqli_num_rows($sql); 
                        if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $hoodie_a=$data['total'];
                        }
                    }else{ // Jika data tidak ada
                        echo 0;
                    }

            ?></td>
        </tr>
        <tr>
            <td class="left">Hoodie Grade B :</td>
            <td id="hoodie_b">
            <?php 
                        $sql = mysqli_query($conn, $query_hb); // Eksekusi/Jalankan query dari variabel $query

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $hoodie_b=$data['total'];
                        }
            ?>
            </td>
        </tr>
        <tr>
            <td class="left">Hoodie Grade C :</td>
            <td id="hoodie_c">
            <?php 
                        $sql = mysqli_query($conn, $query_hc); // Eksekusi/Jalankan query dari variabel $query

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $hoodie_c=$data['total'];
                        }
            ?>
            </td>
        </tr>
        <tr>
            <td class="left">Crewneck Grade A :</td>
            <td id="crewneck_a">
            <?php 
                        $sql = mysqli_query($conn, $query_ca); // Eksekusi/Jalankan query dari variabel $query

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $crewneck_a=$data['total'];
                        }
            ?>
            </td>
        </tr>
        <tr>
            <td class="left">Crewneck Grade B :</td>
            <td id="crewneck_b">
            <?php 
                        $sql = mysqli_query($conn, $query_cb); // Eksekusi/Jalankan query dari variabel $query

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $crewneck_b=$data['total'];
                        }
            ?>
            </td>
        </tr>
        <tr>
            <td class="left">Crewneck Grade C :</td>
            <td id="crewneck_c">
            <?php
                        $sql = mysqli_query($conn, $query_cc); // Eksekusi/Jalankan query dari variabel $query

                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo $data['total'];
                            $crewneck_c=$data['total'];
                        }
            ?>
            </td>
        </tr>
        <tr>
            <td class="left">Total Terjual :</td>
            <td id="total">
                <?php
                    echo $hoodie_a+$hoodie_b+$hoodie_c+$crewneck_a+$crewneck_b+$crewneck_c;
                ?>
            <!-- <script>


                $(document).ready(function(){
                    var ha = document.getElementById("hoodie_a").textContent;
                    var hb = document.getElementById("hoodie_b").textContent;
                    var hc = document.getElementById("hoodie_c").textContent;
                    var ca = document.getElementById("crewneck_a").textContent;
                    var cb = document.getElementById("crewneck_b").textContent;
                    var cc = document.getElementById("crewneck_c").textContent;
                    document.getElementById("total").innerHTML = parseInt(ha)+parseInt(hb)+parseInt(hc)+parseInt(ca)+parseInt(cb)+parseInt(cc); 
                    document.getElementById("pendapatan").innerHTML = "Rp."+" "+(((parseInt(ha)+parseInt(ca))*150000+(parseInt(hb)+parseInt(cb))*75000+(parseInt(hc)+parseInt(cc))*30000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")); 
                });
            </script> -->
            </td>
        </tr>
        <tr>
            <td class="left">Pendapatan Kotor: </td>
            <td id="pendapatan">
                <?php
                    echo "Rp."." ".number_format(($hoodie_a+$crewneck_a)*150000+($hoodie_b+$crewneck_b)*75000+($hoodie_c+$crewneck_c)*30000);
                ?>
            </td>
        </tr>
    </table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('plugin/html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Transaksi.pdf', 'D');
?>
