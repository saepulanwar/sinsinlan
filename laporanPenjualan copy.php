
<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql2="SELECT DISTINCT(nama_produk) FROM daftar_produk";
    $result2=$conn->query($sql2);
    
    
?>
<style>
  .left{
    text-align:right;
  }
</style>

  <div class="row">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <h3 style="text-align:center;">Laporan Penjualan</h3>
            <div class="row">
              <div class="col-md-2" style="background-color:#C7C7C7;"><!-- bagian kiri filter-->
                <form method="post" action="">
                    <label>Filter Berdasarkan</label><br>
                    <select name="filter" id="filter">
                        <option value="">Pilih</option>
                        <option value="1">Per Tanggal</option>
                        <option value="2">Per Bulan</option>
                        <option value="3">Per Tahun</option>
                    </select>
                    <br /><br />

                    <div id="form-tanggal">
                        <label>Tanggal</label><br>
                        <input type="text" name="tanggal" class="input-tanggal" />
                        <br /><br />
                    </div>

                    <div id="form-bulan">
                        <label>Bulan</label><br>
                        <select name="bulan">
                            <option value="">Pilih</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <br /><br />
                    </div>

                    <div id="form-tahun">
                        <label>Tahun</label><br>
                        <select name="tahun">
                            <option value="">Pilih</option>
                            <?php
                            $query = "SELECT YEAR(tanggal) AS tahun FROM barang_keluar GROUP BY YEAR(tanggal)"; // Tampilkan tahun sesuai di tabel transaksi
                            $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query

                            while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                echo '<option value="'.$data['tahun'].'">'.$data['tahun'].'</option>';
                            }
                            ?>
                        </select>
                        <br /><br />
                    </div>
                    
                    <button type="submit" href="?page=stok">Tampilkan</button>
                    <br>
                    <a href="?page=laporanPenjualan">Reset Filter</a>
                </form>
              </div>
              <div class="col-md-10" style="text-align:left;"> <!-- bagian kanan laporan-->
              <!-- here we call the function that makes PDF -->
                
                <div class="row" style="text-align:center;">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">  
                  <div id="HTMLtoPDF">  
                    
                  <h6>
                  <?php
                    if(isset($_POST['filter']) && ! empty($_POST['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
                        $filter = $_POST['filter']; // Ambil data filder yang dipilih user

                        if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                            $tgl = date('d-m-y', strtotime($_POST['tanggal']));

                            echo '<b>Data Transaksi Tanggal '.$tgl.'</b><br /><br />';
                            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE DATE(tanggal)='".$_POST['tanggal']."'";
                     
                            // $query = "SELECT * FROM transaksi WHERE DATE(tgl)='".$_GET['tanggal']."'"; // Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
                        }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                            $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                            echo '<b>Data Transaksi Bulan '.$nama_bulan[$_POST['bulan']].' '.$_POST['tahun'].'</b><br /><br />';
                            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
                            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
                            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
                            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
                            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";
                            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE MONTH(tanggal)='".$_POST['bulan']."'";

                            $query = "SELECT * FROM barang_keluar WHERE MONTH(tgl)='".$_POST['bulan']."' AND YEAR(tgl)='".$_POST['tahun']."'"; // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
                     
                        }else{ // Jika filter nya 3 (per tahun)
                            echo '<b>Data Transaksi Tahun '.$_POST['tahun'].'</b><br /><br />';
                            $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar` WHERE YEAR(tanggal)='".$_POST['tahun']."'";
                            
                            // $query = "SELECT * FROM transaksi WHERE YEAR(tgl)='".$_GET['tahun']."'"; // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
                        }
                    }else{ // Jika user tidak mengklik tombol tampilkan
                        echo '<b>Semua Data Transaksi</b><br /><br />';
                        $query_ha="SELECT SUM(IF(nama_barang = 'hoodie grade a',jumlah, '')) AS total FROM `barang_keluar`";
                        $query_hb="SELECT SUM(IF(nama_barang = 'hoodie grade b',jumlah, '')) AS total FROM `barang_keluar`";
                        $query_hc="SELECT SUM(IF(nama_barang = 'hoodie grade c',jumlah, '')) AS total FROM `barang_keluar`";
                        $query_ca="SELECT SUM(IF(nama_barang = 'crewneck grade a',jumlah, '')) AS total FROM `barang_keluar`";
                        $query_cb="SELECT SUM(IF(nama_barang = 'crewneck grade b',jumlah, '')) AS total FROM `barang_keluar`";
                        $query_cc="SELECT SUM(IF(nama_barang = 'crewneck grade c',jumlah, '')) AS total FROM `barang_keluar`";

                        $query = "SELECT * FROM barang_keluar ORDER BY tgl"; // Tampilkan semua data transaksi diurutkan berdasarkan tanggal

                    }
                    ?>
                     <?php
                                      
                                      $sql2 = "SELECT(SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade A') AS ha, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade b') AS hb, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade C') AS hc, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade A') AS ca, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade b') AS cb, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade c') AS cc";
                                      $sql3="SELECT (SELECT jumlah FROM stok WHERE nama_barang='hoodie grade a') as j_ha, (SELECT jumlah FROM stok WHERE nama_barang='hoodie grade b') as j_hb, (SELECT jumlah FROM stok WHERE nama_barang='hoodie grade c') as j_hc, (SELECT jumlah FROM stok WHERE nama_barang='crewneCk grade a') as j_ca, (SELECT jumlah FROM stok WHERE nama_barang='crewneCk grade B') as j_cb, (SELECT jumlah FROM stok WHERE nama_barang='crewneCk grade c') as j_cc";
                                      $result8=$conn->query($sql3);
                                      $result7 = $conn->query($sql2);
                                      foreach($result8 as $key=>$value){
                                          $j_ha=$value['j_ha'];
                                          $j_hb=$value['j_hb'];
                                          $j_hc=$value['j_hc'];
                                          $j_ca=$value['j_ca'];
                                          $j_cb=$value['j_cb'];
                                          $j_cc=$value['j_cc'];
                                      }
                                  foreach($result7 as $key=>$value){
                                    //   $harga_hoodie=number_format($value['ha']+$value['hb']+$value['hc']);
                                    //   $harga_crewneck=number_format($value['ca']+$value['cb']+$value['cc']);

                                    $h_ha=$value['ha'];
                                    $h_hb=$value['hb'];
                                    $h_hc=$value['hc'];
                                    $h_ca=$value['ca'];
                                    $h_cb=$value['cb'];
                                    $h_cc=$value['cc'];
                                  }
                                  $modal_hoodie=($j_ha*$h_ha)+($j_hb*$h_hb)+($j_hc*$h_hc);
                                  $modal_crewneck=($j_ca*$h_ca)+($j_cb*$h_cb)+($j_cc*$h_cc);
                            ?>
                  </h6>
                    <table>
                        <tr>
                        <td class="left">Nama Barang Terjual</td>
                        <td>Jumlah</td>
   
                        </tr>
                        <tr>
                            <td class="left">Hoodie Grade A :</td>
                            <td id="hoodie_a">
                              <?php 
                                      $sql = mysqli_query($conn, $query_ha); // Eksekusi/Jalankan query dari variabel $query
                                      $row = mysqli_num_rows($sql); 
                                      if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)

                                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $hoodie_a=$data['total'];
                                      }
                                    }else{ // Jika data tidak ada
                                        echo 0;
                                    }

                            ?></td>
                        </tr>
                        <tr>
                            <td class="left">Hoodie Grade B :</td>
                            <td id="hoodie_b">
                            <?php 
                                      $sql = mysqli_query($conn, $query_hb); // Eksekusi/Jalankan query dari variabel $query

                                      while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $hoodie_b=$data['total'];
                                      }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Hoodie Grade C :</td>
                            <td id="hoodie_c">
                            <?php 
                                      $sql = mysqli_query($conn, $query_hc); // Eksekusi/Jalankan query dari variabel $query

                                      while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $hoodie_c=$data['total'];
                                      }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Crewneck Grade A :</td>
                            <td id="crewneck_a">
                            <?php 
                                      $sql = mysqli_query($conn, $query_ca); // Eksekusi/Jalankan query dari variabel $query

                                      while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $crewneck_a=$data['total'];
                                      }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Crewneck Grade B :</td>
                            <td id="crewneck_b">
                            <?php 
                                      $sql = mysqli_query($conn, $query_cb); // Eksekusi/Jalankan query dari variabel $query

                                      while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $crewneck_b=$data['total'];
                                      }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Crewneck Grade C :</td>
                            <td id="crewneck_c">
                            <?php
                                      $sql = mysqli_query($conn, $query_cc); // Eksekusi/Jalankan query dari variabel $query

                                      while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                        //   echo $data['total'];
                                        $crewneck_c=$data['total'];
                                      }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Total Terjual :</td>
                            <td id="total">
                            <script>


                                $(document).ready(function(){
                                  var ha = document.getElementById("hoodie_a").textContent;
                                  var hb = document.getElementById("hoodie_b").textContent;
                                  var hc = document.getElementById("hoodie_c").textContent;
                                  var ca = document.getElementById("crewneck_a").textContent;
                                  var cb = document.getElementById("crewneck_b").textContent;
                                  var cc = document.getElementById("crewneck_c").textContent;
                                  document.getElementById("total").innerHTML = parseInt(ha)+parseInt(hb)+parseInt(hc)+parseInt(ca)+parseInt(cb)+parseInt(cc); 
                                  document.getElementById("pendapatan").innerHTML = "Rp."+" "+(((parseInt(ha)+parseInt(ca))*150000+(parseInt(hb)+parseInt(cb))*75000+(parseInt(hc)+parseInt(cc))*30000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")); 
                                  var pendapatan_hoodie = (parseInt(ha)*150000)+(parseInt(hb)*75000)+(parseInt(hc)*30000);
                                  var pendapatan_crewneck = (parseInt(ca)*150000)+(parseInt(cb)*75000)+(parseInt(cc)*30000);
                                });

                                // .toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Pendapatan Kotor: </td>
                            <td id="pendapatan"></td>
                        </tr>
                        <tr>
                            <td class="left">Pendapatan Bersih Hoodie :</td>
                            <td id="pen_hoodie"></td>
                        
                        </tr>
                        <tr>
                            <td class="left">Pendapatan Bersih Crewneck :</td>
                            <td id="pen_crewneck"></td>
                        
                        </tr>
                        <tr>
                            <td class="left">Pendapatan bersih total:</td>
                            <td id="pendapatan_bersih"></td>
                        </tr>
                    </table>
                    </div>
                  <a href="#" onclick="HTMLtoPDF()">Download PDF</a>   

                  </div> 
                  <div class="col-md-3"></div>
                </div>
              </div>
            </div>
        </div> <!-- container-fluid, round corner -->
    </div>
  </div>

   
<!-- these js files are used for making PDF -->
<script src="js/jspdf.js"></script>
<script src="js/jquery-2.1.3.js"></script>
<script src="js/pdfFromHTML.js"></script>
    <script>
    $(document).ready(function(){ // Ketika halaman selesai di load
        $('.input-tanggal').datepicker({
            dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
        });

        $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

        $('#filter').change(function(){ // Ketika user memilih filter
            if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
                $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
                $('#form-tanggal').show(); // Tampilkan form tanggal
            }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
                $('#form-tanggal').hide(); // Sembunyikan form tanggal
                $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
            }else{ // Jika filternya 3 (per tahun)
                $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
                $('#form-tahun').show(); // Tampilkan form tahun
            }

            $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
        })
    })
    </script>
    <script>
        document.getElementById("hoodie_a").innerHTML =<?php echo $hoodie_a?>;
        document.getElementById("hoodie_b").innerHTML =<?php echo $hoodie_b?>;
        document.getElementById("hoodie_c").innerHTML =<?php echo $hoodie_c?>;
        document.getElementById("crewneck_a").innerHTML =<?php echo $crewneck_a?>;
        document.getElementById("crewneck_b").innerHTML =<?php echo $crewneck_b?>;
        document.getElementById("crewneck_c").innerHTML =<?php echo $crewneck_c?>;

       var ha = document.getElementById("hoodie_a").textContent;
        var hb = document.getElementById("hoodie_b").textContent;
        var hc = document.getElementById("hoodie_c").textContent;
        var pendapatan_hoodie = (parseInt(ha)*150000)+(parseInt(hb)*75000)+(parseInt(hc)*30000);
        var modal_hoodie=<?php echo $modal_hoodie?>;
        // document.getElementById("pen_hoodie").innerHTML = parseInt(pendapatan_hoodie)-parseInt(modal_hoodie);
        document.getElementById("pen_hoodie").innerHTML = "Rp."+" "+((parseInt(pendapatan_hoodie)-parseInt(modal_hoodie)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

        var ca = document.getElementById("crewneck_a").textContent;
        var cb = document.getElementById("crewneck_b").textContent;
        var cc = document.getElementById("crewneck_c").textContent;
        var pendapatan_crewneck = (parseInt(ca)*150000)+(parseInt(cb)*75000)+(parseInt(cc)*30000);
        var modal_crewneck=<?php echo $modal_crewneck?>;
        // document.getElementById("pen_crewneck").innerHTML = parseInt(pendapatan_crewneck)-parseInt(modal_crewneck);
        document.getElementById("pen_crewneck").innerHTML = "Rp."+" "+((parseInt(pendapatan_crewneck)-parseInt(modal_crewneck)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

        var ph = parseInt(pendapatan_hoodie)-parseInt(modal_hoodie);
        var pc = parseInt(pendapatan_crewneck)-parseInt(modal_crewneck);
        document.getElementById("pendapatan_bersih").innerHTML = "Rp."+" "+((parseInt(ph)+parseInt(pc)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    </script>
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script> <!-- Load file plugin js jquery-ui -->

