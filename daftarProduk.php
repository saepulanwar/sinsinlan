<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT * FROM daftar_produk";
    
    $result = $conn->query($sql);
?>



<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

    .horizontal {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;
}

</style>

<div class="row horizontal">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            
            <table class="table" id="myTable">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Produk</th>
                        <th scope="col">Harga Jual</th>
                        <?php if($_SESSION['akun_level']=='admin'){?>
                            <th scope="col">*</th>
                        <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            $num = 1;
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $num++;?></th>
                            <td><?php echo $row['nama_produk'];?></td>
                            <td><?php echo $row['harga_jual'];?></td>
                                <?php
                                if($_SESSION['akun_level']=='admin'){
                                    echo 
                                    "<td align='center'>
                                    <a id='edit_data' data-toggle='modal' data-target='#edit-data' data-id_produk='".$row['id']."'"."data-nama_produk='".$row['nama_produk']."'"."data-harga_jual='".$row['harga_jual']."'".">
                                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                                    </a>
                                    <a id='hapus_data' data-toggle='modal' data-target='#hapus-data' data-id_produk='".$row['id']."'"."data-nama_produk='".$row['nama_produk']."'".">
                                        <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-delete'></i>Hapus</button>
                                    </a>
                                    </td>";
                                }
                                ?>
                        </tr>
                        <?php
                            }
                        } else {
                            echo "0 results";
                        }
                        // $conn->close();
                        ?>
                </tbody>
            </table> 
        </div>
    </div>
</div>

<!-- modal tambah data -->
<div class="modal fade" id="tambah-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Form Tambah Produk Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-tambah" enctype="multipart/form-data">
            <div class="modal-body" id="modal-tambah">
                <!-- <div class="form-group">
                    <label for="id_barang">ID Barang</label>
                    <input type="text" id="id_barang" name="id_barang" class="form-control" required>
                </div> -->
                <div class="form-group">
                    <label for="nama_produk">Nama Produk</label>
                    <!-- <input type="text" id="barang" name="barang" class="form-control" required> -->
                    <input type="text" id="nama_produk" name="nama_produk" class="form-control" placeholder="masukan nama produk tanpa grade nya">
                </div>
                <div class="form-group">
                    <label for="grade">Grade Produk</label>
                    <!-- <input type="text" id="barang" name="barang" class="form-control" required> -->
                    <input type="text" id="grade" name="grade" class="form-control" placeholder="masukan grade produk, contoh: &quot;A&quot; atau &quot;B&quot;">
                </div>
                <div class="form-group">
                    <label for="harga_jual">Harga Jual</label>
                    <input type="number" id="harga_jual" name="harga_jual" class="form-control" required>
                    <input type="hidden" id="nama_user" name="nama_user" value="<?php echo $_SESSION['akun_nama'];?>">
                </div>
                <!-- <div class="form-group">
                    <label for="harga">Harga Per Bal</label>
                    <input type="number" id="harga" name="harga" class="form-control" required>
                </div> -->
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
            </div>
        </form>
    </div>
    </div>
</div>


<!-- Modal pop up edit data -->
<div class="modal fade" id="edit-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data Penduduk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_edit_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <!-- <div class="form-group">
                <label for="tanggal">Tanggal Penjualan</label>
                <input type="date" name="tanggal" class="form-control" id="tanggal" required>
                <input  type="hidden" id="id_data" name="id_data">
            </div> -->
            <div class="form-group">
                <label for="nama_produk">Nama Produk</label>
                 <input type="text" name="nama_produk" class="form-control" id="nama_produk" disabled>
            </div>
            <div class="form-group">
                <label for="harga_jual">Harga Jual</label>
                <input type="number" name="harga_jual" class="form-control" id="harga_jual" required>
                <input  type="hidden" id="id_produk" name="id_produk">
                <input type="hidden" id="grade" name="grade">
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>


<!-- MODAL HAPUS DATA -->

<div class="modal fade" id="hapus-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Anda Yakin?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_hapus_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-hapus">
        
            <div class="form-group">
                <label for="nip">Yakin anda akan menghapus produk  <b><span name="nama_produk" id="nama_produk"></b> </label>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input  type="hidden" id="id_produk" name="id_produk">
                <input  type="hidden" id="nama_produk" name="nama_produk">
                <input  type="hidden" id="nama_barang" name="nama_barang">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Yakin">
        </div>
        </form>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// penilaian


// TAMBAH DATA
$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'data_produk.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=daftarProduk";
    }));
    
});



// EDIT DATA

$(document).on("click", "#edit_data", function() {
    var id_produk = $(this).data('id_produk');
    var nama_produk = $(this).data('nama_produk');
    var harga_jual = $(this).data('harga_jual');
    var grade = $(this).data('grade');

    $("#modal-edit #id_produk").val(id_produk);
    $("#modal-edit #nama_produk").val(nama_produk);
    $("#modal-edit #harga_jual").val(harga_jual);

})
$(document).ready(function(e) {
    $("#form_edit_data").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_data_produk.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=daftarProduk";
    }));
});


// HAPUS DATA
$(document).on("click", "#hapus_data", function() {

    var id_produk = $(this).data('id_produk');
    var nama_produk = $(this).data('nama_produk');

    $("#modal-hapus #id_produk").val(id_produk);
    $("#modal-hapus #nama_produk").text(nama_produk);
})

$(document).ready(function(e) {
$("#form_hapus_data").on("submit", (function(e) {
e.preventDefault();
$.ajax({
    url:'hapus_data_produk.php',
    type: 'POST',
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    success: function(msg) {
    $('.table').html(msg);
    }
});
window.location="?page=daftarProduk";
}));
});
</script>