<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT * FROM barang_keluar ORDER BY tanggal ASC";
    
    $result = $conn->query($sql);

    $sql2="SELECT DISTINCT(nama_barang) FROM barang_masuk";
    $result2=$conn->query($sql2);
    // foreach($result as $key=>$value){
    //     echo $value['nama'];
    // }
?>
<?php
    if(isset($_POST['tanggal']) && ! empty($_POST['tanggal'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
        // $filter = $_POST['filter']; // Ambil data filder yang dipilih user
        $tgl = date('d/m/y', strtotime($_POST['tanggal']));
        $tgl2 = date('d/m/y', strtotime($_POST['tanggal2']));
        echo '<b>Data Transaksi Tanggal '.$tgl.' - '.$tgl2.'</b><br /><br />';
            // Jika filter nya 1 (per tanggal)
        $sql = "SELECT * FROM barang_keluar WHERE DATE(tanggal)>='".$_POST['tanggal']."' AND DATE(tanggal)<='".$_POST['tanggal2']."' ";
        $result = $conn->query($sql);
        
            // $query = "SELECT * FROM transaksi WHERE DATE(tgl)='".$_GET['tanggal']."'"; // Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
        
    }else{ // Jika user tidak mengklik tombol tampilkan

        $sql = "SELECT * FROM barang_keluar ORDER BY tanggal ASC";
        $result = $conn->query($sql);
        echo '<b>Semua Data Transaksi</b><br /><br />';

        // Tampilkan semua data transaksi diurutkan berdasarkan tanggal
    }
?>

<?php
                                      
            $sql2 = "SELECT(SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade A') AS ha, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade b') AS hb, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Hoodie Grade C') AS hc, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade A') AS ca, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade b') AS cb, (SELECT harga_satuan FROM `barang_masuk` WHERE tanggal IN (SELECT max(tanggal) FROM barang_masuk) AND nama_barang = 'Crewneck Grade c') AS cc";
            $result7 = $conn->query($sql2);
        foreach($result7 as $key=>$value){
            // echo $value['ha'];
            // echo "<br>";
            // echo $value['cc'];
        }
            
?>

<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

    .horizontal {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;
}

</style>

<div class="row horizontal">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <p>Masukan rentang tanggal untuk melihat data berdasarkan tanggal!</p>
            <span class="row" style="margin-left: 10px; margin-bottom: 10px;">
                <form method="post" action="">
                    <div class="row">
                        <div style="margin-right:20px; margin-left:20px;" class="row" id="form-tanggal">
                            <!-- <label>Tanggal</label><br> -->
                            <input style="height:30px;" type="text" name="tanggal" class="input-tanggal" />
                            <br /><br />
                        </div>
                        <div class="row" id="form-tanggal2">
                            <label>-</label><br>
                            <input style="height:30px;" type="text" name="tanggal2" class="input-tanggal" />
                            <br /><br />
                        </div>                    
                        <button style="height:30px; margin-left:30px; margin-right:20px;" type="submit" href="?page=barangKeluar">Tampilkan</button>
                        <br>
                        <a href="?page=barangKeluar">Reset Filter</a>
                    </div>
                </form>
            </span>
            <table class="table" id="myTable">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tanggal Penjualan</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Jumlah</th>
                        <!-- <th scope="col">Total</th> -->
                        <!-- <th scope="col">Harga Per Bal</th>
                        <th scope="col">Harga per Unit</th> -->
                        <th scope="col">Nama Karyawan</th>
                        <?php if($_SESSION['akun_level']=='admin'){?>
                            <th scope="col">*</th>
                        <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            $num = 1;
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $num++;?></th>
                            <td><?php echo $row['tanggal'];?></td>
                            <td><?php echo $row['nama_barang'];?></td>
                            <td><?php echo $row['jumlah'];?></td>
                            <td><?php echo $row['nama_user'];?></td>
                                <?php
                                if($_SESSION['akun_level']=='admin'){
                                    echo 
                                    "<td align='center'>
                                    <a id='edit_data' data-toggle='modal' data-target='#edit-data' data-id_barang_keluar='".$row['id_barang_keluar']."'"."data-tanggal='".$row['tanggal']."'"."data-nama_barang='".$row['nama_barang']."'"."data-jumlah='".$row['jumlah']."'"."data-nama_user='".$row['nama_user']."'".">
                                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                                    </a>
                                    <a id='hapus_data' data-toggle='modal' data-target='#hapus-data' data-id_barang_keluar='".$row['id_barang_keluar']."'"."data-tanggal='".$row['tanggal']."'"."data-nama_barang='".$row['nama_barang']."'"."data-jumlah='".$row['jumlah']."'"."data-nama_user='".$row['nama_user']."'".">
                                        <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-delete'></i>Hapus</button>
                                    </a>
                                    </td>";
                                }
                                ?>
                        </tr>
                        <?php
                            }
                        } else {
                            echo "0 results";
                        }
                        // $conn->close();
                        ?>
                </tbody>
            </table> 
        </div>
    </div>
</div>

<!-- modal tambah data -->
<div class="modal fade" id="tambah-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Form Data Penjualan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-tambah" enctype="multipart/form-data">
            <div class="modal-body" id="modal-tambah">
                <!-- <div class="form-group">
                    <label for="id_barang">ID Barang</label>
                    <input type="text" id="id_barang" name="id_barang" class="form-control" required>
                </div> -->
                <div class="form-group">
                    <label for="barang">Nama Barang</label>
                    <!-- <input type="text" id="barang" name="barang" class="form-control" required> -->
                    <select class="dropdown form-control" id = "barang" name="barang">
                    <?php
                    foreach($result2 as $key=>$value){
                        echo "<option value='".$value['nama_barang']."'>".$value['nama_barang']."</option>";
                    }
                    ?>
                        <!-- <option value="" disabled>Pilih Nama Barang</option>
                        <option value = "Hoodie Grade A">Hoodie Grade A</option>
                        <option value = "Hoodie Grade B">Hoodie Grade B</option>
                        <option value = "Hoodie Grade C">Hoodie Grade C</option>
                        <option value = "Crewneck Grade A">Crewneck Grade A</option>
                        <option value = "Crewneck Grade B">Crewneck Grade B</option>
                        <option value = "Crewneck Grade C">Crewneck Grade C</option> -->
                    </select>
                </div>
                <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" id="jumlah" name="jumlah" class="form-control" required>
                    <input type="hidden" id="nama_user" name="nama_user" value="<?php echo $_SESSION['akun_nama'];?>">
                </div>
                <!-- <div class="form-group">
                    <label for="harga">Harga Per Bal</label>
                    <input type="number" id="harga" name="harga" class="form-control" required>
                </div> -->
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
            </div>
        </form>
    </div>
    </div>
</div>


<!-- Modal pop up edit data -->
<div class="modal fade" id="edit-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data Penduduk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_edit_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <!-- <div class="form-group">
                <label for="tanggal">Tanggal Penjualan</label>
                <input type="date" name="tanggal" class="form-control" id="tanggal" required>
                <input  type="hidden" id="id_data" name="id_data">
            </div> -->
            <div class="form-group">
                <label for="nama_barang">Nama Barang</label>
                    <select class="dropdown form-control" id = "nama_barang" name="nama_barang">
                        <option value="" disabled>Pilih Nama Barang</option>
                        <option value = "Hoodie Grade A">Hoodie Grade A</option>
                        <option value = "Hoodie Grade B">Hoodie Grade B</option>
                        <option value = "Hoodie Grade C">Hoodie Grade C</option>
                        <option value = "Crewneck Grade A">Crewneck Grade A</option>
                        <option value = "Crewneck Grade B">Crewneck Grade B</option>
                        <option value = "Crewneck Grade C">Crewneck Grade C</option>
                    </select>
            </div>
            <div class="form-group">
                <label for="jumlah">Jumlah</label>
                <input type="number" name="jumlah" class="form-control" id="jumlah" required>
                <input type="hidden" name="jumlah_awal" class="form-control" id="jumlah_awal" required>
                <input  type="hidden" id="id_data" name="id_data">
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>


<!-- MODAL HAPUS DATA -->

<div class="modal fade" id="hapus-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Anda Yakin?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form_hapus_data" enctype="multipart/form-data">
        <div class="modal-body" id="modal-hapus">
        
            <div class="form-group">
                <label for="nip">Yakin anda akan menghapus data dengan nama <p name="nama" id="nama"></p> </label>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input  type="hidden" id="id_data" name="id_data">
                <input  type="hidden" id="jumlah" name="jumlah">
                <input  type="hidden" id="nama_barang" name="nama_barang">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Yakin">
        </div>
        </form>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">

// TAMBAH DATA
$(document).on("click", "#tambah_data", function() {
    var barang = $(this).data('barang');
    var harga = $(this).data('harga');
    var jumlah = $(this).data('jumlah');
})

$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'data_barang_keluar.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=barangKeluar";
    }));
    
});
function caritanggal() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("tanggal_1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

function filterRows() {
  var from = $('#datefilterfrom').val();
  var to = $('#datefilterto').val();

  if (!from && !to) { // no value for from and to
    return;
  }

  from = from || '1970-01-01'; // default from to a old date if it is not set
  to = to || '2999-12-31';

  var dateFrom = moment(from);
  var dateTo = moment(to);

  $('#test tr').each(function(i, tr) {
    var val = $(tr).find("td:nth-child(2)").text();
    var dateVal = moment(val, "DD/MM/YYYY");
    var visible = (dateVal.isBetween(dateFrom, dateTo, null, [])) ? "" : "none"; // [] for inclusive
    $(tr).css('display', visible);
  });
}

$('#datefilterfrom').on("change", filterRows);
$('#datefilterto').on("change", filterRows);
// $(function() {
//   $('input[name="daterange"]').daterangepicker({
//     opens: 'left'
//   }, function(start, end, label) {
//     console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
//   });
// });


// EDIT DATA

$(document).on("click", "#edit_data", function() {
    var id_barang_keluar = $(this).data('id_barang_keluar');
    var nama_barang = $(this).data('nama_barang');
    var tanggal = $(this).data('tanggal');
    var jumlah = $(this).data('jumlah');
    var jumlah_awal = $(this).data('jumlah');
    $("#modal-edit #id_data").val(id_barang_keluar);
    $("#modal-edit #nama_barang").val(nama_barang);
    $("#modal-edit #tanggal").val(tanggal);
    $("#modal-edit #jumlah").val(jumlah);
    $("#modal-edit #jumlah_awal").val(jumlah);

})
$(document).ready(function(e) {
    $("#form_edit_data").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_barang_keluar.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=barangKeluar";
    }));
});


// HAPUS DATA
$(document).on("click", "#hapus_data", function() {

    var id_barang_keluar = $(this).data('id_barang_keluar');
    var jumlah = $(this).data('jumlah');
    var nama = $(this).data('nama_barang');

    $("#modal-hapus #id_data").val(id_barang_keluar);
    $("#modal-hapus #jumlah").val(jumlah);
    $("#modal-hapus #nama").text(nama);
    $("#modal-hapus #nama_barang").val(nama);
})

$(document).ready(function(e) {
$("#form_hapus_data").on("submit", (function(e) {
e.preventDefault();
$.ajax({
    url:'hapus_barang_keluar.php',
    type: 'POST',
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    success: function(msg) {
    $('.table').html(msg);
    }
});
window.location="?page=barangKeluar";
}));
});
</script>

<script>
    $(document).ready(function(){ // Ketika halaman selesai di load
        $('.input-tanggal').datepicker({
            dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
        });
    })
    </script>

<script src="plugin/jquery-ui/jquery-ui.min.js"></script>