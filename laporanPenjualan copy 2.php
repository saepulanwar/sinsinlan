
<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql2="SELECT nama_produk FROM daftar_produk";
    $result2=$conn->query($sql2);
    
    
?>
<style>
  .left{
    text-align:right;
  }
</style>

  <div class="row">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <h3 style="text-align:center;">Laporan Penjualan</h3>
            <div class="row">
              <div class="col-md-2" style="background-color:#C7C7C7;"><!-- bagian kiri filter-->
                <form method="post" action="">
                    <label>Filter Berdasarkan</label><br>
                    <select name="filter" id="filter">
                        <option value="">Pilih</option>
                        <option value="1">Per Tanggal</option>
                        <option value="2">Per Bulan</option>
                        <option value="3">Per Tahun</option>
                    </select>
                    <br /><br />

                    <div id="form-tanggal">
                        <label>Tanggal</label><br>
                        <input type="text" name="tanggal" class="input-tanggal" />
                        <br /><br />
                    </div>

                    <div id="form-bulan">
                        <label>Bulan</label><br>
                        <select name="bulan">
                            <option value="">Pilih</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <br /><br />
                    </div>

                    <div id="form-tahun">
                        <label>Tahun</label><br>
                        <select name="tahun">
                            <option value="">Pilih</option>
                            <?php
                            $query = "SELECT YEAR(tanggal) AS tahun FROM barang_keluar GROUP BY YEAR(tanggal)"; // Tampilkan tahun sesuai di tabel transaksi
                            $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query

                            while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                echo '<option value="'.$data['tahun'].'">'.$data['tahun'].'</option>';
                            }
                            ?>
                        </select>
                        <br /><br />
                    </div>
                    
                    <button type="submit" href="?page=stok">Tampilkan</button>
                    <br>
                    <a href="?page=laporanPenjualan">Reset Filter</a>
                </form>
              </div>
              <div class="col-md-10" style="text-align:left;"> <!-- bagian kanan laporan-->
              <!-- here we call the function that makes PDF -->
                
                <div class="row" style="text-align:center;">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">  
                  <div id="HTMLtoPDF">  
                    
                  <h6>
                  <?php
                    if(isset($_POST['filter']) && ! empty($_POST['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
                        $filter = $_POST['filter']; // Ambil data filder yang dipilih user
                        if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                            $tgl = date('d-m-y', strtotime($_POST['tanggal']));
                            echo '<b>Data Transaksi Tanggal '.$tgl.'</b><br /><br />';
                            // $query = "SELECT * FROM transaksi WHERE DATE(tgl)='".$_GET['tanggal']."'"; // Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
                        }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                            $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            echo '<b>Data Transaksi Bulan '.$nama_bulan[$_POST['bulan']].' '.$_POST['tahun'].'</b><br /><br />';
                        // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
                        }else{ // Jika filter nya 3 (per tahun)
                            echo '<b>Data Transaksi Tahun '.$_POST['tahun'].'</b><br /><br />';
                            
                            // $query = "SELECT * FROM transaksi WHERE YEAR(tgl)='".$_GET['tahun']."'"; // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
                        }
                    }else{ // Jika user tidak mengklik tombol tampilkan
                        echo '<b>Semua Data Transaksi</b><br/><br />';
                    }
                    ?>
                  </h6>
                    <table>
                        <tr>
                        <td class="left">Nama Barang Terjual</td>
                        <td>Jumlah</td>
   
                        </tr>
                                <?php 
                                
                                  foreach($result2 as $key=>$value){?>
                                    <tr>
                                        <td class='left'><?php echo $value['nama_produk']." :";?></td>
                                        <td>
                                            <?php


                                            if(isset($_POST['filter']) && ! empty($_POST['filter'])){
                                                if($filter=="1"){
                                                    $tanggal1=$_POST['tanggal'];
                                                    $namaProduk=$value['nama_produk'];
                                                    $sql3 = "SELECT SUM(IF(nama_barang = '$namaProduk',jumlah, '')) AS total FROM barang_keluar WHERE DATE(tanggal)='$tanggal1'";
                                                    $result3=$conn->query($sql3);
                                                    foreach($result3 as $n=>$nilai){
                                                        if(empty($nilai['total'])){
                                                            echo "0";
                                                        }else{
                                                        echo $nilai['total'];
                                                        }
                                                    }
                                                }else if($filter=="2"){
                                                    $bulan1=$_POST['bulan'];
                                                    $tahun1=$_POST['tahun'];
                                                    $namaProduk=$value['nama_produk'];
                                                    $sql4="SELECT SUM(IF(nama_barang = '$namaProduk', jumlah, '')) AS total FROM barang_keluar WHERE MONTH(tanggal)='$bulan1' AND YEAR(tanggal)='$tahun1'";
                                                    $result4=$conn->query($sql4);
                                                    foreach($result4 as $v=>$val){
                                                        if(empty($val['total'])){
                                                            echo "0";
                                                        }else{
                                                            echo $val['total'];
                                                        }
                                                    }

                                                }else if($filter=="3"){
                                                    $tahun2=$_POST['tahun'];
                                                    $namaProduk=$value['nama_produk'];
                                                    $sql5="SELECT SUM(IF(nama_barang = '$namaProduk', jumlah, '')) AS total FROM barang_keluar WHERE YEAR(tanggal)='$tahun2'";
                                                    $result5=$conn->query($sql5);
                                                    foreach($result5 as $v=>$val){
                                                        if(empty($val['total'])){
                                                            echo "0";
                                                        }else{
                                                            echo $val['total'];
                                                        }
                                                    }

                                                }
                                           
                                        }else{
                                            
                                            $namaProduk=$value['nama_produk'];
                                            $sql3 = "SELECT SUM(IF(nama_barang = '$namaProduk',jumlah, '')) AS total FROM barang_keluar";
                                            $result3=$conn->query($sql3);
                                            foreach($result3 as $n=>$nilai){
                                                if(empty($nilai['total'])){
                                                    // echo "0";
                                                echo $nilai['total'];

                                                }else{
                                                echo $nilai['total'];
                                                }
                                            }
                                        }
                                            ?>
                                        </td>
                                    </tr>
                                 <?php }
                                ?>
                        <tr>
                            <td class="left">Total Terjual : </td>
                            <td id="total">
                            <!-- jumlah -->
                            <?php
                             if(isset($_POST['filter']) && ! empty($_POST['filter'])){
                                if($filter == "1"){
                                    $tanggal2=$_POST['tanggal'];
                                    $sql6="SELECT SUM(jumlah) AS total FROM barang_keluar WHERE DATE(tanggal)='$tanggal2'";
                                    $result6=$conn->query($sql6);
                                    foreach($result6 as $row=>$value){
                                        if(empty($value['total'])){
                                            echo "0";
                                        }else{
                                        echo $value['total'];
                                        }
                                    }
                                }else if($filter=="2"){
                                    $bulan2=$_POST['bulan'];
                                    $tahun3=$_POST['tahun'];
                                    $sql6="SELECT SUM(jumlah) AS total FROM barang_keluar WHERE MONTH(tanggal)='$bulan2' AND YEAR(tanggal)='$tahun3'";
                                    $result6=$conn->query($sql6);
                                    foreach($result6 as $row=>$value){
                                        if(empty($value['total'])){
                                            echo "0";
                                        }else{
                                        echo $value['total'];
                                        }
                                    }
                                }else if($filter == "3"){
                                    $tahun3=$_POST['tahun'];
                                    $sql6="SELECT SUM(jumlah) AS total FROM barang_keluar WHERE YEAR(tanggal)='$tahun3'";
                                    $result6=$conn->query($sql6);
                                    foreach($result6 as $row=>$value){
                                        if(empty($value['total'])){
                                            echo "0";
                                        }else{
                                        echo $value['total'];
                                        }
                                    }
                                }
                             }else{
                                 $sql6="SELECT SUM(jumlah) AS total FROM barang_keluar";
                                 $result6=$conn->query($sql6);
                                 foreach($result6 as $row=>$value){
                                    if(empty($value['total'])){
                                        echo "0";
                                    }else{
                                    echo $value['total'];
                                    }
                                }
                            }
                            ?>
                            <!-- <script>
                                $(document).ready(function(){
                                  var ha = document.getElementById("hoodie_a").textContent;
                                  var hb = document.getElementById("hoodie_b").textContent;
                                  var hc = document.getElementById("hoodie_c").textContent;
                                  var ca = document.getElementById("crewneck_a").textContent;
                                  var cb = document.getElementById("crewneck_b").textContent;
                                  var cc = document.getElementById("crewneck_c").textContent;
                                  document.getElementById("total").innerHTML = parseInt(ha)+parseInt(hb)+parseInt(hc)+parseInt(ca)+parseInt(cb)+parseInt(cc); 
                                  document.getElementById("pendapatan").innerHTML = "Rp."+" "+(((parseInt(ha)+parseInt(ca))*150000+(parseInt(hb)+parseInt(cb))*75000+(parseInt(hc)+parseInt(cc))*30000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")); 
                                  var pendapatan_hoodie = (parseInt(ha)*150000)+(parseInt(hb)*75000)+(parseInt(hc)*30000);
                                  var pendapatan_crewneck = (parseInt(ca)*150000)+(parseInt(cb)*75000)+(parseInt(cc)*30000);
                                });

                                // .toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            </script> -->
                            </td>
                        </tr>
                        <tr>
                            <td class="left">Pendapatan Kotor : </td>
                            <td id="pendapatan">
                            <?php
                             if(isset($_POST['filter']) && ! empty($_POST['filter'])){
                                if($filter == "1"){
                                    $tanggal3=$_POST['tanggal'];
                                    $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE DATE(tanggal)='$tanggal3'";
                                 $result6=$conn->query($sql6);

                                 foreach($result6 as $row=>$value){
                                    $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                }
                                if(empty($arrPdp)){
                                    echo "Rp. 0";
                                }else{
                                    $pendapatan = array_sum($arrPdp);
                                    $totalPendapatan = number_format($pendapatan);
                                    echo "Rp.".$totalPendapatan;
                                }
                           
                                }else if($filter=="2"){
                                    $bulan2=$_POST['bulan'];
                                    $tahun3=$_POST['tahun'];
                                    $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE YEAR(tanggal)='$tahun3' AND MONTH(tanggal)='$bulan2'";
                                    $result6=$conn->query($sql6);
   
                                    foreach($result6 as $row=>$value){
                                       $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                   }
                                   if(empty($arrPdp)){
                                       echo "Rp. 0";
                                   }else{
                                       $pendapatan = array_sum($arrPdp);
                                       $totalPendapatan = number_format($pendapatan);
                                       echo "Rp.".$totalPendapatan;
                                   }
                                }else if($filter == "3"){
                                    $tahun3=$_POST['tahun'];
                                    $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE YEAR(tanggal)='$tahun3'";
                                 $result6=$conn->query($sql6);

                                 foreach($result6 as $row=>$value){
                                    $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                }
                                if(empty($arrPdp)){
                                    echo "Rp. 0";
                                }else{
                                    $pendapatan = array_sum($arrPdp);
                                    $totalPendapatan = number_format($pendapatan);
                                    echo "Rp.".$totalPendapatan;
                                }
                                }
                             }else{
                                 $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang";
                                 $result6=$conn->query($sql6);

                                 foreach($result6 as $row=>$value){
                                    $arrPdpt[] = $value['harga_jual']*$value['jumlah'];
                                }
                                $pendapatan = array_sum($arrPdpt);
                                $totalPendapatan = number_format($pendapatan);
                                echo "Rp.".$totalPendapatan;
                            }
                            ?>
                            </td>
                        </tr>
                        <?php 
                        if(isset($_POST['filter']) && ! empty($_POST['filter'])){
                            if($filter=="1"){
                                $tanggal=$_POST['tanggal'];
                                $sql8="SELECT SUM((harga_jual*barang_keluar.jumlah)) AS pendapatan_kotor,SUM(harga_bal) AS pengeluaran, SUBSTRING_INDEX(barang_keluar.nama_barang, 'Grade', 1) AS nama_barang FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang JOIN barang_masuk ON barang_keluar.nama_barang=barang_masuk.nama_barang WHERE DATE(barang_keluar.tanggal)='$tanggal' GROUP BY SUBSTRING(nama_produk, 1, 4)";   
                            }else if($filter=="2"){
                                $bulan=$_POST['bulan'];
                                $tahun=$_POST['tahun'];
                                $sql8="SELECT SUM((harga_jual*barang_keluar.jumlah)) AS pendapatan_kotor,SUM(harga_bal) AS pengeluaran, SUBSTRING_INDEX(barang_keluar.nama_barang, 'Grade', 1) AS nama_barang FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang JOIN barang_masuk ON barang_keluar.nama_barang=barang_masuk.nama_barang WHERE MONTH(barang_keluar.tanggal)='$bulan' AND YEAR(barang_keluar.tanggal)='$tahun' GROUP BY SUBSTRING(nama_produk, 1, 4)";  
                            }else if($filter=="3"){
                                $tahun=$_POST['tahun'];
                                $sql8="SELECT SUM((harga_jual*barang_keluar.jumlah)) AS pendapatan_kotor,SUM(harga_bal) AS pengeluaran, SUBSTRING_INDEX(barang_keluar.nama_barang, 'Grade', 1) AS nama_barang FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang JOIN barang_masuk ON barang_keluar.nama_barang=barang_masuk.nama_barang WHERE YEAR(barang_keluar.tanggal)='$tahun' GROUP BY SUBSTRING(nama_produk, 1, 4)";  
                            }
                        }else{
                        $sql8="SELECT SUM((harga_jual*barang_keluar.jumlah)) AS pendapatan_kotor,SUM(harga_bal) AS pengeluaran, SUBSTRING_INDEX(barang_keluar.nama_barang, 'Grade', 1) AS nama_barang FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang JOIN barang_masuk ON barang_keluar.nama_barang=barang_masuk.nama_barang  GROUP BY SUBSTRING(nama_produk, 1, 4)";       
                        }
                        $result8=$conn->query($sql8);
                        foreach($result8 as $data=>$mantap){
                            $pendapatanB = number_format($mantap['pendapatan_kotor']-$mantap['pengeluaran']);

                        
                        ?>
                        <tr>
                            <td class="left">Pendapatan Bersih <?php echo $mantap['nama_barang']; ?> :</td>
                            <td id="pen_hoodie"><?php echo "Rp. ".$pendapatanB;?> </td>
                        
                        </tr>
                        <?php }?>
                        <tr>
                            <td class="left">Pendapatan bersih total :</td>
                            <td id="pendapatan_bersih">
                                <?php
                             if(isset($_POST['filter']) && ! empty($_POST['filter'])){
                                 if($filter=="1"){
                                        $tanggal3=$_POST['tanggal'];

                                    $sql7="SELECT SUM(harga_bal) total_pengeluaran FROM barang_masuk WHERE DATE(tanggal)='$tanggal3'";
                                    $result7=$conn->query($sql7);
                                    foreach($result7 as $key=>$value){
                                        $pengeluaran = $value['total_pengeluaran'];
                                    }

                                        $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE DATE(tanggal)='$tanggal3'";
                                     $result6=$conn->query($sql6);
    
                                     foreach($result6 as $row=>$value){
                                        $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                    }
                                    if(empty($arrPdp)){
                                        $pendapatan = 0; 
                                    }else{
                                        $pendapatan = array_sum($arrPdp); 
                                    }
                                    $pendapatanBersih = number_format($pendapatan-$pengeluaran);
                                    echo "Rp. ".$pendapatanBersih;
                                 }else if($filter=="2"){
                                    $bulan3=$_POST['bulan'];
                                    $tahun3=$_POST['tahun'];

                                        $sql7="SELECT SUM(harga_bal) total_pengeluaran FROM barang_masuk WHERE YEAR(tanggal)='$tahun3' AND MONTH(tanggal)='$bulan3'";
                                        $result7=$conn->query($sql7);
                                        foreach($result7 as $key=>$value){
                                            $pengeluaran = $value['total_pengeluaran'];
                                        }

                                            $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE YEAR(tanggal)='$tahun3' AND MONTH(tanggal)='$bulan3'";
                                        $result6=$conn->query($sql6);

                                        foreach($result6 as $row=>$value){
                                            $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                        }
                                        if(empty($arrPdp)){
                                            $pendapatan = 0; 
                                        }else{
                                            $pendapatan = array_sum($arrPdp); 
                                        }
                                        $pendapatanBersih = number_format($pendapatan-$pengeluaran);
                                        echo "Rp. ".$pendapatanBersih;
                                    }else if($filter=="3"){
                                    $tahun3=$_POST['tahun'];

                                        $sql7="SELECT SUM(harga_bal) total_pengeluaran FROM barang_masuk WHERE YEAR(tanggal)='$tahun3'";
                                        $result7=$conn->query($sql7);
                                        foreach($result7 as $key=>$value){
                                            $pengeluaran = $value['total_pengeluaran'];
                                        }

                                            $sql6="SELECT * FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang WHERE YEAR(tanggal)='$tahun3'";
                                        $result6=$conn->query($sql6);

                                        foreach($result6 as $row=>$value){
                                            $arrPdp[] = $value['harga_jual']*$value['jumlah'];  
                                        }
                                        if(empty($arrPdp)){
                                            $pendapatan = 0; 
                                        }else{
                                            $pendapatan = array_sum($arrPdp); 
                                        }
                                        $pendapatanBersih = number_format($pendapatan-$pengeluaran);
                                        echo "Rp. ".$pendapatanBersih;
                                    }
                             }else{
                                    $sql7="SELECT SUM(harga_bal) total_pengeluaran FROM barang_masuk";
                                    $result7=$conn->query($sql7);
                                    foreach($result7 as $key=>$value){
                                        $pengeluaran = $value['total_pengeluaran'];
                                    }
                                    $sql6="SELECT (barang_keluar.jumlah*harga_jual) AS pendapatan FROM daftar_produk INNER JOIN barang_keluar ON daftar_produk.nama_produk = barang_keluar.nama_barang";
                                    $result6=$conn->query($sql6);
   
                                    foreach($result6 as $row=>$value){
                                       $arrPdpt = ($value['pendapatan']);
                                   }
                                //    var_dump($arrPdpt);
                                //    $pendapatan = array_sum($arrPdpt);
                                //    var_dump($arrPdpt);
                                   
                                   echo "Rp. ".number_format($pendapatan-$pengeluaran);

                                //    $pendapatanBersih = number_format($pendapatan-$pengeluaran);
                                //    echo "Rp. ".number_format($pendapatan-$pengeluaran);
                                }


                                ?>
                            </td>
                        </tr>
                    </table>
                    </div>
                  <a href="#" onclick="HTMLtoPDF()">Download PDF</a>   

                  </div> 
                  <div class="col-md-3"></div>
                </div>
              </div>
            </div>
        </div> <!-- container-fluid, round corner -->
    </div>
  </div>

   
<!-- these js files are used for making PDF -->
<script src="js/jspdf.js"></script>
<script src="js/jquery-2.1.3.js"></script>
<script src="js/pdfFromHTML.js"></script>
    <script>
    $(document).ready(function(){ // Ketika halaman selesai di load
        $('.input-tanggal').datepicker({
            dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
        });

        $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

        $('#filter').change(function(){ // Ketika user memilih filter
            if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
                $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
                $('#form-tanggal').show(); // Tampilkan form tanggal
            }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
                $('#form-tanggal').hide(); // Sembunyikan form tanggal
                $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
            }else{ // Jika filternya 3 (per tahun)
                $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
                $('#form-tahun').show(); // Tampilkan form tahun
            }

            $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
        })
    })
    </script>
    
    <script src="plugin/jquery-ui/jquery-ui.min.js"></script> <!-- Load file plugin js jquery-ui -->

