<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT 
    IF(nama_barang='hoodie grade a',jumlah,'') 
    AS hoodie_a,
    IF(nama_barang='hoodie grade b',jumlah,'') 
    AS hoodie_b,
    IF(nama_barang='hoodie grade C',jumlah,'') 
    AS hoodie_c,
    IF(nama_barang='Crewneck grade a',jumlah,'') 
    AS crewneck_a,
    IF(nama_barang='Crewneck grade B',jumlah,'') 
    AS crewneck_b,
    IF(nama_barang='Crewneck grade C',jumlah,'') 
    AS crewneck_c
    FROM stok";
    
    
    $result = $conn->query($sql);
    // foreach($result as $key=>$value){
    //     echo $value['nama'];
    // }
    $sql2="SELECT 
	SUM(IF(nama_barang LIKE 'hoodie%', jumlah, ''))
    AS total_hoodie,
    SUM(IF(nama_barang LIKE 'crewneck%', jumlah, ''))
    AS total_crewneck
    FROM stok";
    $result2 = $conn->query($sql2);
?>

<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 40%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 0px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  /* tr:nth-child(even) {
    background-color: #dddddd;
  } */
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */
.left{
    text-align:right;
}

    /* .horizontal {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;
} */

</style>

<div class="row horizontal">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <h3 style="text-align:center;">Laporan Penjualan</h3>
            <h6>Tanggal Penjualan:</h6>
            <table>
                <tr>
                <td>Nama Barang Terjual:</td>
                </tr>
                <tr>
                    <td class="left">Hoodie Grade A :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td class="left">Hoodie Grade B :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td class="left">Hoodie Grade C :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td class="left">Crewneck Grade A :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td class="left">Crewneck Grade B :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td class="left">Crewneck Grade C :</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td>Total Terjual :</td>
                    <td>138</td>
                </tr>
                <tr>
                    <td>Pendapatan Kotor: </td>
                    <td>Rp.<?php echo ' '.number_format(150000*46+75000*46+30000*46);?></td>
                </tr>
            </table>
            <!-- <h6>Tanggal Penjualan :</h6>
            <p>Nama Barang Terjual:</p>
            <p>1. Hoodie Grade A: 23</p>
            <p>2. Hoodie Grade B: 23</p>
            <p>3. Hoodie Grade C: 23</p>
            <p>4. Crewneck Grade A: 23</p>
            <p>5. Crewneck Grade B: 23</p>
            <p>6. Crewneck Grade C: 23</p>
            <p>Total Terjual: 138</p>
            <p>Pendapatan Kotor : Rp. 1231312</p> -->
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// penilaian

$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'data_barang_keluar.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=barangKeluar";
    }));
    
});
function caritanggal() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("tanggal_1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

function filterRows() {
  var from = $('#datefilterfrom').val();
  var to = $('#datefilterto').val();

  if (!from && !to) { // no value for from and to
    return;
  }

  from = from || '1970-01-01'; // default from to a old date if it is not set
  to = to || '2999-12-31';

  var dateFrom = moment(from);
  var dateTo = moment(to);

  $('#test tr').each(function(i, tr) {
    var val = $(tr).find("td:nth-child(2)").text();
    var dateVal = moment(val, "DD/MM/YYYY");
    var visible = (dateVal.isBetween(dateFrom, dateTo, null, [])) ? "" : "none"; // [] for inclusive
    $(tr).css('display', visible);
  });
}

$('#datefilterfrom').on("change", filterRows);
$('#datefilterto').on("change", filterRows);
// $(function() {
//   $('input[name="daterange"]').daterangepicker({
//     opens: 'left'
//   }, function(start, end, label) {
//     console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
//   });
// });
</script>