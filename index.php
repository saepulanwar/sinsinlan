<?php
ob_start();
session_start();
if(!isset($_SESSION['akun_id'])) header("location: login.php");
include "config.php";


?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SINSINLAN - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <link rel="stylesheet" href="plugin/jquery-ui/jquery-ui.min.css" /> <!-- Load file css jquery-ui -->
  <script src="js/jquery.min.js"></script> <!-- Load file jquery -->

  <script src='build/pdfmake.min.js'></script>
  <script src='build/vfs_fonts.js'></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-white sidebar sidebar-light accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-balance-scale"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SINSINLAN</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="?page=">
        <i class="fas fa-home"></i>
          <span>Home</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item active">
        <a class="nav-link" href="?page=barangMasuk">
        <i class="fas fa-sign-in-alt"></i>
          <span>Barang Masuk</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item active">
        <a class="nav-link" href="?page=barangKeluar">
        <i class="fas fa-sign-out-alt"></i>
          <span>Barang Keluar</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item active">
        <a class="nav-link" href="?page=stok">
        <i class="fas fa-fw fa-chart-area"></i>
          <span>Data Stok</span></a>
      </li>

      
      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item active">
        <a class="nav-link" href="?page=laporanPenjualan">
        <i class="fas fa-fw fa-chart-area"></i>
          <span>Laporan Penjualan</span></a>
      </li>
      <?php if ($_SESSION['akun_level'] == 'admin'){
        // echo 'ss';
        ?>
      <!-- Divider -->
      <hr class="sidebar-divider">
        
      <li class="nav-item active">
        <a class="nav-link" href="?page=daftarProduk">
        <i class="fas fa-fw fa-chart-area"></i>
          <span>Daftar Produk</span></a>
      </li>

      <hr class="sidebar-divider">

      <li class="nav-item active">
        <a class="nav-link" href="?page=dataUser">
        <i class="fas fa-users"></i>
          <span>Data User</span></a>
      </li>
      <?php }?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['akun_nama'];?></span>
                <img class="img-profile rounded-circle" src="images/<?php echo $_SESSION['akun_foto'];?>">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div> -->
                <a class="dropdown-item" href="logout.php">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">
            
            <?php
              $page = @$_GET['page'];
              if($page == ""){
                echo "Home";
              }else if($page == "barangMasuk"){
                echo "Data Barang Masuk  <a id='tambah_data' data-toggle='modal' data-target='#tambah-data' onClick='tambahData()'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a>";
              }else if($page == "barangKeluar"){
                echo "Data Barang Keluar  <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a>";
              }
              else if($page == "dataUser"){
                echo "Data User <a id='tambah_user' data-toggle='modal' data-target='#tambah-user'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah User</button></a>";
              }
              else if($page == "stok"){
                echo "Data Stok Barang";
              }
              else if($page == "laporanPenjualan"){
                echo "Laporan Penjualan";
              }else if($page == "daftarProduk"){
                echo "Daftar Produk <a id='tambah_produk' data-toggle='modal' data-target='#tambah-produk'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Produk</button></a>";
              }

            ?>
            </h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>
          <?php
									$page = @$_GET['page'];
									if($page == ""){
									include "home.php";
									}else if($page == "barangKeluar"){
									include "barangKeluar.php";
									}else if($page == "barangMasuk"){
									include "barangMasuk.php";
									}
									else if($page == "stok"){
									include "stok.php";
									}
									else if($page == "dataUser"){
									include "dataUser.php";
                  }
                  else if($page == "laporanPenjualan"){
                  include "laporanPenjualan.php";
                  }else if($page == "daftarProduk"){
                    include "daftarProduk.php";
                  }
								?>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <small>Rancangan tugas akhir</small>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- PRINT 2 PDF -->
<!-- <script src="js/jspdf.js"></script>
<script src="js/jquery-2.1.3"></script>
<script src="js/pdfFromHTML"></script> -->



  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- date picker -->
  <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />            -->

  <script type="text/javascript">

  $(document).ready(function(){
      $('#mytable td.status').each(function(){
          if ($(this).text() == 'sehat') {
              $(this).css('background-color','#53FCBA');
          }else if ($(this).text() == 'sakit') {
              $(this).css('background-color','#FE776D');
          }
      });
  });

  </script>
  

    <script>
    // $(document).ready(function(){ // Ketika halaman selesai di load
    //     $('.input-tanggal').datepicker({
    //         dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
    //     });

    //     $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

    //     $('#filter').change(function(){ // Ketika user memilih filter
    //         if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
    //             $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
    //             $('#form-tanggal').show(); // Tampilkan form tanggal
    //         }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
    //             $('#form-tanggal').hide(); // Sembunyikan form tanggal
    //             $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
    //         }else{ // Jika filternya 3 (per tahun)
    //             $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
    //             $('#form-tahun').show(); // Tampilkan form tahun
    //         }

    //         $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
    //     })
    // })
    </script>
</body>

</html>
