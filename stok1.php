<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT 
    IF(nama_barang='hoodie grade a',jumlah,'') 
    AS hoodie_a,
    IF(nama_barang='hoodie grade b',jumlah,'') 
    AS hoodie_b,
    IF(nama_barang='hoodie grade C',jumlah,'') 
    AS hoodie_c,
    IF(nama_barang='Crewneck grade a',jumlah,'') 
    AS crewneck_a,
    IF(nama_barang='Crewneck grade B',jumlah,'') 
    AS crewneck_b,
    IF(nama_barang='Crewneck grade C',jumlah,'') 
    AS crewneck_c
    FROM stok";
    
    
    $result = $conn->query($sql);
    // foreach($result as $key=>$value){
    //     echo $value['nama'];
    // }
    $sql2="SELECT 
	SUM(IF(nama_barang LIKE 'hoodie%', jumlah, ''))
    AS total_hoodie,
    SUM(IF(nama_barang LIKE 'crewneck%', jumlah, ''))
    AS total_crewneck
    FROM stok";
    $result2 = $conn->query($sql2);

    $sql3="SELECT * FROM stok WHERE nama_barang LIKE 'Hoodie%'";
    $result3=$conn->query($sql3);

    $sql4="SELECT * FROM stok WHERE nama_barang LIKE 'Crewneck%'";
    $result4=$conn->query($sql4);
?>

<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */

    /* .horizontal {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;
} */

</style>

<div class="row horizontal">
    <div class="col-md-12">
    <!-- <a id='tambah_data' data-toggle='modal' data-target='#tambah-data'><button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah Data</button></a> -->
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <h3 style="text-align:center;">Stok Barang Saat Ini</h3>
            <div class="row" style="padding:10px;">
                <div class="col-md-6" style="border-right:solid 5px #fff; background-color:#E0E0E0; border-radius:10px;">

                    <h5 style="text-align:center;">Hoodie</h5>
                    <div class="row" style="padding:10px;">
                      <table>
                        <?php foreach($result3 as $key=>$value){
                          echo "<tr>
                                  <td>".$value['nama_barang'].":</td>
                                  <td>".$value['jumlah']."</td>
                                </tr>";
                        }

                          ?>
                      
                          <tr>
                            <th>Total          :</th>
                            <?php foreach($result2 as $key=>$value){?>
                            <td><?php echo $value['total_hoodie'];?></td>
                            <?php }?>
                          </tr>
                      </table>
                        

                        
                    </div>
                </div>
                <!-- <div class="col-md-6" style="border-right:solid 5px #fff; background-color:#E0E0E0; border-radius:10px;">

                    <h5 style="text-align:center;">Crewneck</h5>
                    <div class="row" style="padding:10px;">
                    <table>
                        <?php foreach($result4 as $key=>$value){
                          echo "<tr>
                                  <td>".$value['nama_barang'].":</td>
                                  <td>".$value['jumlah']."</td>
                                </tr>";
                        }

                          ?>
                      
                          <tr>
                            <th>Total          :</th>
                            <?php foreach($result2 as $key=>$value){?>
                            <td><?php echo $value['total_crewneck'];?></td>
                            <?php }?>
                          </tr>
                      </table>
                    </div>
                </div>
            </div> -->
           
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// penilaian

$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'data_barang_keluar.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=barangKeluar";
    }));
    
});
function caritanggal() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("tanggal_1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

function filterRows() {
  var from = $('#datefilterfrom').val();
  var to = $('#datefilterto').val();

  if (!from && !to) { // no value for from and to
    return;
  }

  from = from || '1970-01-01'; // default from to a old date if it is not set
  to = to || '2999-12-31';

  var dateFrom = moment(from);
  var dateTo = moment(to);

  $('#test tr').each(function(i, tr) {
    var val = $(tr).find("td:nth-child(2)").text();
    var dateVal = moment(val, "DD/MM/YYYY");
    var visible = (dateVal.isBetween(dateFrom, dateTo, null, [])) ? "" : "none"; // [] for inclusive
    $(tr).css('display', visible);
  });
}

$('#datefilterfrom').on("change", filterRows);
$('#datefilterto').on("change", filterRows);
// $(function() {
//   $('input[name="daterange"]').daterangepicker({
//     opens: 'left'
//   }, function(start, end, label) {
//     console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
//   });
// });
</script>