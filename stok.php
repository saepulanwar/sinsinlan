<?php  
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sisinlan";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT SUBSTRING_INDEX(nama_barang, 'Grade', 1) AS nama_barang, SUM(jumlah) AS jumlah  FROM stok GROUP BY SUBSTRING(nama_barang, 1,4)";
    $result = $conn->query($sql);

    $sql2 = "SELECT *, SUBSTRING_INDEX(nama_barang, 'Grade', 1) AS nama_kelompok FROM stok";
    $result2 = $conn->query($sql2);



?>

<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 10px;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  .table-title{
    padding-top:10px;
  }

  .info a {
    color: #ffff; 
    } /* CSS link color */
</style>

<div class="row horizontal">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px; padding-top:20px; padding-bottom:10px;">
            <h3 style="text-align:center;">Stok Barang Saat Ini</h3>
            <div class="row" style="padding:10px;">
            <?php 
            foreach($result as $key=>$value){?>
                <div class="col-md-6" style="border-right:solid 5px #fff; background-color:#E0E0E0; border-radius:10px; margin-bottom:20px;">
                    <h5 style="text-align:center;"><?php echo $value['nama_barang'];?></h5>
                    <div class="row" style="padding:10px;">
                      <table>
                      <?php foreach($result2 as $baris=>$nilai){
                          if($value['nama_barang']==$nilai['nama_kelompok']){?>
                          <tr>
                          <td><?php echo $nilai['nama_barang'];?></td>
                          <td><?php echo $nilai['jumlah'];?></td>
                          
                          </tr>
                      <?php } } ?>
                        <tr>
                          <td>Total :</td>
                          <td><?php echo $value['jumlah'];?></td>
                        </tr>
                      </table>
                    </div>
                </div>
            <?php } ?>
          </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
</script>